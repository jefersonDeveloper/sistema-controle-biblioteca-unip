# Sistema Controle Biblioteca
Sistema de gerenciamento de controle de bibliotecas da Universidade Paulista.

## Instalação do projeto
### `npm install`
### `npm start`
### `adonis migration:run; adonis seed`
### Rota para documentação usando Swagger `http://localhost:3000/docs`
