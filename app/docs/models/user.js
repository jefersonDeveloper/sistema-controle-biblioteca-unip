/**
*  @swagger
*  definitions:
*    User:
*      type: object
*      properties:
*        id:
*          type: int
*        nome:
*          type: string
*        email:
*          type: string
*        senha:
*          type: string
*        telefone:
*          type: string
*      required:
*        - id
*        - nome
*        - email
*        - senha
*/