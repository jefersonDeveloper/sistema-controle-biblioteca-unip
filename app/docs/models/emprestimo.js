/**
*  @swagger
*  definitions:
*    Emprestimo:
*      type: object
*      properties:
*        id:
*          type: int
*        data_inicio:
*          type: timestamp
*        data_final:
*          type: timestamp
*        data_devolucao:
*          type: timestamp
*      required:
*        - id
*        - data_inicio
*        - data_final
*        - data_devolucao
*/