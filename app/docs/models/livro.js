/**
*  @swagger
*  definitions:
*    Livro:
*      type: object
*      properties:
*        id:
*          type: int
*        titulo:
*          type: string
*        quantidade:
*          type: int
*        foto:
*          type: string
*      required:
*        - id
*        - titulo
*        - quantidade
*/