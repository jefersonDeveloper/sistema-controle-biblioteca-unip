'use strict'

const Livro = use('App/Models/Livro');
const Autor = use('App/Models/Autor');
const { validate } = use('Validator');

class LivroController {

    async index({ request, response }) {
        const offset = request.input('offset')
        const limit = request.input('limit')

        const livros = await Livro
            .query()
            .with('autor')
            .paginate(offset, limit)

        response.send({
            'code': 200,
            'status': 'success',
            'data': livros
        })
    }

    async show({ params, response }) {
        const id = params.id
        const livro = await Livro.find(id)

        response.send({
            'code': 200,
            'status': 'success',
            'data': livro
        })
    }

    async store({ request, response }) {

        const rules = {
            titulo: 'required',
            quantidade: 'required',
            foto: 'required',
            nome: 'required'
        }

        const validation = await validate(request.all(), rules)

        if (validation.fails()) {

            return response.ok(validation.messages())
        }

        const requestLivro = {
            'titulo': request.input('titulo'),
            'quantidade': request.input('quantidade'),
            'foto': request.input('foto'),
        }

        const requestAutor = {
            'nome': request.input('nome')
        }

        const livro = new Livro();
        const autor = new Autor();
        livro.fill(requestLivro);
        await livro.save();
        autor.fill(requestAutor);
        await livro.autor().save(autor);

        response.send({
            'code': 201,
            'status': 'success',
            'data': livro
        })
    }

    async update({ params, request, response }) {
        const id = params.id;
        const all = request.all();
        const livro = await Livro.query()
            .where('id', id).update(all)

        response.send({
            'code': 200,
            'status': 'success',
            'data': livro
        });
    }

    async delete({ params, response }) {
        const id = params.id
        const livro = await Livro.find(id);
        await livro.autor().detach();
        await livro.emprestimo().detach();
        await livro.delete();

        response.send({
            'code': 200,
            'status': 'success',
            'data': livro
        });
    }
}

module.exports = LivroController
