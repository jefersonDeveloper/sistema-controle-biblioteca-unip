'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Hash = use('Hash')

Factory.blueprint('App/Models/Livro', async (faker) => {
    return {
        titulo: faker.word(),
        quantidade: faker.integer({ min: 2, max: 2000 }),
        foto: faker.hash({ length: 25 }) + '.jpg'
    }
});

Factory.blueprint('App/Models/Autor', async (faker) => {
    return {
        nome: faker.name()
    }
});

Factory.blueprint('App/Models/Emprestimo', async (faker) => {
    return {
        user_id: faker.integer({ min: 1, max: 3 }),
        data_inicio: faker.date(),
        data_final: faker.date(),
        data_devolucao: faker.date(),
    }
});


Factory.blueprint('App/Models/User', async (faker) => {
    return {
        nome: faker.name(),
        email: faker.email(),
        password: Hash.make(faker.word()),
        telefone: faker.phone(),
    }
});


