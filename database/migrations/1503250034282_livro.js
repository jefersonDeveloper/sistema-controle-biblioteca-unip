'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LivroSchema extends Schema {
  up() {
    this.create('livros', (table) => {
      table.increments()
      table.string('titulo', 200).notNullable()
      table.integer('quantidade').notNullable()
      table.text('foto')
      table.timestamps()
    })
  }

  down() {
    this.drop('livros')
  }
}

module.exports = LivroSchema
