'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmprestimoSchema extends Schema {
  up() {
    this.create('emprestimos', (table) => {
      table.increments()
      table.integer('user_id').notNullable().references('id').inTable('users')
      table.date('data_inicio').notNullable()
      table.date('data_final').notNullable()
      table.date('data_devolucao').notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('emprestimos')
  }
}

module.exports = EmprestimoSchema
